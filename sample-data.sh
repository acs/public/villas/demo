#!/bin/sh

set +e

ACTION=${1:-restore}
FILE=${2:-demo_database_backup_$(date +%Y-%m-%d).zip}

NAME=$(basename $(pwd))
VOLUME="${NAME}_files"

VOLUME_PATH=$(docker volume inspect ${VOLUME} | jq -r .[0].Mountpoint)

DOCKEROPTS="--interactive --tty --rm --network ${NETWORK} --volume $(pwd):/tmp"

TEMP_PATH=$(mktemp -d)

echo "Temp path: ${TEMP_PATH}"
echo "Volume path: ${VOLUME_PATH}"

case ${ACTION} in
  restore)
    tar -C ${TEMP_PATH} -x -v -f ${FILE}
    docker-compose exec database mongo VILLAS --eval "db.dropDatabase()"
    docker-compose exec -T database mongorestore --archive < ${TEMP_PATH}/database.mongoarchive

    if [ -f ${TEMP_PATH}/files.tar.gz ]; then
        rm -rf ${VOLUME_PATH}/*
        tar -C ${VOLUME_PATH} -x -z -v -f ${TEMP_PATH}/files.tar.gz
    fi
    ;;

  dump)
    # docker exec mangles stdout and stderr output. So we must use --quiet
    docker-compose exec -T database mongodump --archive --quiet > ${TEMP_PATH}/database.mongoarchive
    tar -C ${VOLUME_PATH} -c -z -v -f ${TEMP_PATH}/files.tar.gz .
    tar -C ${TEMP_PATH} -c -v -f ${FILE} .
    ;;

  *)
    echo "Usage: $0 (restore|dump) [FILE]"
    ;;
esac

#rm -rf ${TMP_PATH}
