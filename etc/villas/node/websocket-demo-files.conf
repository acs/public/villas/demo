/** Example configuration file for VILLASnode.
 *
 * The syntax of this file is similar to JSON.
 * A detailed description of the format can be found here:
 *   http://www.hyperrealm.com/libconfig/libconfig_manual.html#Configuration-Files
 *
 * @author Steffen Vogel <stvogel@eonerc.rwth-aachen.de>
 * @copyright 2017, Institute for Automation of Complex Power Systems, EONERC
 * @license GNU General Public License (version 3)
 *
 * VILLASnode
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

stats = 2;

nodes = {
	file_ssA = {
		type = "file",

		in = {
#			uri = "https://OOKc0UfswqvgRp0@rwth-aachen.sciebo.de/public.php/webdav/inl-csu/ssA_cosim_inl-csu_20170726_03-28-21.csv",
			uri = "/villas/data/GenericCoSimExmp/inl-csu/ssA_cosim_inl-csu_20170726_03-28-21.csv"
			eof = "rewind"
		}
	},
	file_ssB = {
		type = "file",

		in = {
#			uri = "https://OOKc0UfswqvgRp0@rwth-aachen.sciebo.de/public.php/webdav/inl-csu/ssB_cosim_inl-csu_20170726_03-27-19.csv",
			uri = "/villas/data/GenericCoSimExmp/inl-csu/ssB_cosim_inl-csu_20170726_03-27-19.csv"
			eof = "rewind"
		}
	}
	sig_1 = {
		type = "signal",
		
		signal = "mixed",
		values = 4,
		rate = 50,
		frequency = 0.5
		stddev = 0.2
	},
	ws_sig = {
		type = "websocket",
		vectorize = 10,
	},
	ws_ssA = {
		type = "websocket",
		vectorize = 1,
	},
	ws_ssB = {
		type = "websocket",
		vectorize = 1,
	}
};

############       List of paths       ############

paths = (
	{ in = "sig_1", out = "ws_sig", hooks = ( { type = "stats" } ) },

	{ in = "file_ssA", out = "ws_ssA", hooks = ( { type = "decimate", ratio = 200 } ) },
	{ in = "file_ssB", out = "ws_ssB", hooks = ( { type = "decimate", ratio = 200 } ) }
);
