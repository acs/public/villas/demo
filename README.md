# VILLASframework Demo

This repository contains a fully working configuration of VILLASframework.

An online deployment of this demo is available at: https://web.villas.fein-aachen.org

## Usage

We are using Docker and Docker Compose in order to make it as easy as possible for you to get started.

1. [Install Docker](https://docs.docker.com/engine/installation/).
2. Clone repository.
3. Execute `git submodule update --init --recursive`
4. Extract the files.
5. Open the Docker CLI and navigate to the location of the demo files.
6. Start demo by running `docker-compose up -d`
7. VILLASweb is available via `127.0.0.1:80`

## Backup / Restore Database

We provide a simple shell script to backup and restore the current state of the MongoDB database:

- Backup: `./sample-data.sh dump backup_2018_08_30.tar`
- Restore: `./sample-data.sh restore backup_2018_08_30.tar`

## Copyright

2017-2018, Institute for Automation of Complex Power Systems, EONERC

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](doc/pictures/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Steffen Vogel <stvogel@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
